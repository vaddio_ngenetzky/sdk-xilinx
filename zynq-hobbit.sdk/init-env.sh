XILINX_VERSION='2017.4'
SDKBIN_DIR="/opt/Xilinx/SDK/${XILINX_VERSION}/bin/"
if [ ! -d ${SDKBIN_DIR} ] ; then
    echo 'Unable to locate SDK bin'
else
    export PATH="${SDKBIN_DIR}:${PATH}"
fi

