#!/opt/Xilinx/SDK/2017.4/bin/xsct

sdk setws ./
# sdk importprojects ./

proc make_hw {hdf_file} {
    if {[file exists "hw"] == 0} {
        sdk createhw \
            -name hw \
            -hwspec $hdf_file
    } else {
        sdk updatehw \
            -hw hw \
            -newhwspec $hdf_file
    }
}

proc make_dt {} {
    if {[file exists "dt"] == 0} {
        # add the xilinx device-tree repository for BSP generation
        sdk set_user_repo_path \
            ../device-tree-xlnx
        sdk createbsp \
            -name dt \
            -hwproject hw \
            -proc ps7_cortexa9_0 \
            -os device_tree
    } else {
        puts "Please delete the 'dt' project first."
    }
}

proc make_fsbl {} {
    if {[file exists "fsbl"] == 0} {
        sdk createapp \
            -name fsbl \
            -proc ps7_cortexa9_0 \
            -hwproject hw \
            -os standalone \
            -app "Zynq FSBL"
    } else {
        puts "Please delete the 'fsbl' project first."
    }
}

make_hw "system.hdf"
make_dt
make_fsbl


