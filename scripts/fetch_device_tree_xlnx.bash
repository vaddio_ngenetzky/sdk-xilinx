
DEFAULT_VERSION='2017.4'
GITROOT="${GITROOT-$(readlink -f ./$(git rev-parse --show-cdup))}"

fetch_device_tree_xlnx(){
    local version="${1-${DEFAULT_VERSION}}"
    local tag="xilinx-v${version}"
    local gitdir="${GITROOT}/device-tree-xlnx"

    if [ ! -d ${gitdir} ]; then
        git clone 'git@bitbucket.org:vaddio/device-tree-xlnx.git' "${gitdir}"
    fi
    git -C "${gitdir}" fetch origin
    git -C "${gitdir}" checkout "${tag}"
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
    # Bash Strict Mode
    set -eu -o pipefail

    set -x
    fetch_device_tree_xlnx "$@"
fi

